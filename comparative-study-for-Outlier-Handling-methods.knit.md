---
title: A comparative study on Univariate Outliers Handling methods in Data Science
  Context
author: "Iyad Alkrunz"
date: "12/26/2021"
output:
  pdf_document: default
  word_document: default
---
 




Abstract

Outliers are values in data that differ extremely from a major sample of the data, the presence of outliers can significantly reduce the performance and accuracy of a predictable model.

**Keywords**
Capping; Flooring; Outlier; Quantile-based. 

# Introduction

The measure of how good a machine learning model depends on how clean the data is, and the presence of outliers may be as a result of errors during the collection of data,


# Outliers

## sources of outliers
## impact of outliers on statistical analysis
## Outliers detection methods
There are different ways and methods of identifying outliers but in this paper we are only going to use Interquartile Range Method.The interquartile range is a measure of statistical dispersion and is calculated as the difference between 75th and 25th percentiles. the Quartiles divide the data set into four equal parts. The values that separate parts are called the first, second, and third quartiles.


delete 
## Common methods on handling outliers



# Methods of handling univariate outliers

## Replace outliers by mean
In this technique the outliers replaced with the mean value that is a central value of a finite set of numbers specifically, the sum of the values divided by the number of values. 

## Replace outliers by median
In this technique the outliers replaced with the median value that is the middle number in a sorted, ascending or descending

## Replace outliers by mode
In this technique the outliers replaced with the mode value that appears most often in a set of data values.

## Quantile-based Flooring and Capping
in this quantile-based technique, we will do the flooring(25th percentile) for the lower values and capping(for the 75th percentile) for the higher values. These percentile values will be used for the quantile-based flooring and capping.







# Simulation (Numerical Study)

A R program has been developed and implemented in R Studio environment to generate data from different distribution namely normal , Negative Binomial and Exponential Distribution, Data were generated with four different sample sizes namely,20,50,100 and 200. 
each generated data set is contaminated by $\varepsilon ={0.1, 0.2, 0.3}$ from different distribution as explained in the following sub sections



## Settings

### Normal Distribution
For Normal Distribution data were generated from standard normal distribution with $\mu = 0$ and $\sigma = 1$, and for contamination procedure, The Contaminated data were generated from another normal distribution with $\mu = 4$ and $\sigma = 2$  

The maximum likelihood estimator (mle)  of the mean is obtained as the sample mean $\hat{\mu}_{mle} = \bar{x} = \frac{1}{n} \sum_{i=1}^n x_i$

and sd .......


### Negative Binomial Distribution
Random variable $X$ follows The Negative Binomial distribution $X \sim NB(r,p)$ with mean $\mu =\frac{r}{p}$ and variance $\sigma^2= \frac{r(1-p)}{p^2}$ if $X$ is the count of independent Bernoulli trials required to achieve the $r^{th}$ successful trial when the probability of success is constant $p$. The probability of $X=n$ trials is $f(X=n)= {n-1 \choose r-1} p^r (1-p)^{n-r}$
For Negative Binomial Distribution data were generated with parameters $r=2 , p=0.2$, The Contaminated data were generated from Poisson distribution  with $\lambda = 32$ 
 
from negative binomial distributions with parameters prob=p and size=${k}$, mle of p is given by: $$\hat{p}_{mle} = \frac{K}{X + K}$$




### Exponential distribution
The Exponential distribution is the most commonly used model in reliability & life-testing analysis, data were generated with parameter  $\lambda = 0.5$, and for contamination procedure Contaminated data were generated from Exponential distribution with $\lambda = 0.05$ with Contamination levels $\varepsilon ={0.1, 0.2, 0.3}$  

 The maximum likelihood estimator (mle) of $\lambda$ is given by: $$\hat{λ}_{mle} = \frac{1}{\bar{x}}$$ Where $\bar{x} = \frac{1}{n}\sum^n_{i=1} x_i$ That is, the mle is the reciprocal of the sample mean.



For each combination of the considered probability distribution,sample size and contamination level and handling methods the generation procedures are repeated 1000 iterations to ensure convergence.
 

 
## Performance indicators
The performance of the considered four handling outliers methods are mesured by three common indicators as follows

1. Bias is the difference between this estimator's expected value and the true value of the parameter being estimated.

2. MSE is a measure of the quality of an estimator. As it is derived from the square of Euclidean distance, it is always a positive value that decreases as the error approaches zero.

3. Goodness of fit tests are statistical tests aiming to determine whether a set of observed values match those expected under the applicable model. There are multiple types of goodness-of-fit tests,In this study we used Shipiro-Wilk test to determines if a sample follows a normal distribution, and we used Kolmogorov-Smirnov Test to determines if a sample follows a Negative Binomial Distribution and used Chi-Square Test to determines if a sample follows a Exponential distribution




## Results
this section presents result of Simulation for each distribution,

### Normal Distribution
in normal distribution we focused on mean and standard deviation, we calculated bias and mean squared error for each estimator after applying the four different methods of handling outliers, with different sample sizes and different contamination levels, also we applied  Goodness of fit test namely Shipiro-Wilk test to determines if sample follows a normal distribution






\begin{table}

\caption{\label{tab:unnamed-chunk-3}Table 1: Bias of the normal distribution mean estimator for different handling-outliers methods}
\centering
\begin{tabular}[t]{r|r|r|r|r|r}
\hline
\multicolumn{2}{c|}{ } & \multicolumn{4}{c}{Handling-outliers Methods} \\
\cline{3-6}
n & epsilon & Quantile-based & Mean & Median & Mode\\
\hline
20 & 10 & 0.234 & 0.097 & 0.078 & 0.073\\
\hline
50 & 10 & 0.243 & 0.086 & 0.068 & 0.062\\
\hline
100 & 10 & 0.244 & 0.085 & 0.069 & 0.062\\
\hline
200 & 10 & 0.246 & 0.085 & 0.070 & 0.063\\
\hline
20 & 20 & 0.491 & 0.261 & 0.216 & 0.198\\
\hline
50 & 20 & 0.508 & 0.248 & 0.206 & 0.187\\
\hline
100 & 20 & 0.510 & 0.242 & 0.201 & 0.181\\
\hline
200 & 20 & 0.509 & 0.235 & 0.195 & 0.174\\
\hline
20 & 30 & 0.772 & 0.511 & 0.457 & 0.433\\
\hline
50 & 30 & 0.787 & 0.480 & 0.426 & 0.398\\
\hline
100 & 30 & 0.796 & 0.488 & 0.437 & 0.410\\
\hline
200 & 30 & 0.796 & 0.483 & 0.434 & 0.405\\
\hline
\end{tabular}
\end{table}

![](comparative-study-for-Outlier-Handling-methods_files/figure-latex/unnamed-chunk-3-1.pdf)<!-- --> \begin{table}

\caption{\label{tab:unnamed-chunk-3}Table 2: Bias of the normal distribution standared error estimator for different handling-outliers methods}
\centering
\begin{tabular}[t]{r|r|r|r|r|r}
\hline
n & epsilon & Quantile-based & Mean & Median & Mode\\
\hline
20 & 10 & 0.230 & 0.003 & 0.008 & 0.002\\
\hline
50 & 10 & 0.253 & 0.003 & 0.006 & 0.002\\
\hline
100 & 10 & 0.253 & 0.005 & 0.002 & 0.006\\
\hline
200 & 10 & 0.255 & 0.011 & 0.008 & 0.010\\
\hline
20 & 20 & 0.483 & 0.132 & 0.116 & 0.125\\
\hline
50 & 20 & 0.519 & 0.142 & 0.131 & 0.137\\
\hline
100 & 20 & 0.526 & 0.142 & 0.133 & 0.138\\
\hline
200 & 20 & 0.530 & 0.139 & 0.130 & 0.135\\
\hline
20 & 30 & 0.773 & 0.406 & 0.389 & 0.398\\
\hline
50 & 30 & 0.799 & 0.378 & 0.366 & 0.374\\
\hline
100 & 30 & 0.820 & 0.408 & 0.400 & 0.407\\
\hline
200 & 30 & 0.822 & 0.405 & 0.398 & 0.404\\
\hline
\end{tabular}
\end{table}

![](comparative-study-for-Outlier-Handling-methods_files/figure-latex/unnamed-chunk-3-2.pdf)<!-- --> \begin{table}

\caption{\label{tab:unnamed-chunk-3}Table 3: MSE of the normal distribution mean estimator for different handling-outliers methods}
\centering
\begin{tabular}[t]{r|r|r|r|r|r}
\hline
n & epsilon & Quantile-based & Mean & Median & Mode\\
\hline
20 & 10 & 0.105 & 0.065 & 0.064 & 0.065\\
\hline
50 & 10 & 0.080 & 0.030 & 0.028 & 0.028\\
\hline
100 & 10 & 0.070 & 0.019 & 0.017 & 0.017\\
\hline
200 & 10 & 0.066 & 0.013 & 0.011 & 0.010\\
\hline
20 & 20 & 0.299 & 0.137 & 0.123 & 0.121\\
\hline
50 & 20 & 0.281 & 0.088 & 0.072 & 0.067\\
\hline
100 & 20 & 0.273 & 0.072 & 0.056 & 0.049\\
\hline
200 & 20 & 0.265 & 0.062 & 0.045 & 0.039\\
\hline
20 & 30 & 0.661 & 0.349 & 0.311 & 0.297\\
\hline
50 & 30 & 0.648 & 0.269 & 0.227 & 0.208\\
\hline
100 & 30 & 0.647 & 0.258 & 0.215 & 0.194\\
\hline
200 & 30 & 0.641 & 0.244 & 0.201 & 0.179\\
\hline
\end{tabular}
\end{table}

![](comparative-study-for-Outlier-Handling-methods_files/figure-latex/unnamed-chunk-3-3.pdf)<!-- --> \begin{table}

\caption{\label{tab:unnamed-chunk-3}Table 4: MSE of the normal distribution standared error estimator for different handling-outliers methods}
\centering
\begin{tabular}[t]{r|r|r|r|r|r}
\hline
n & epsilon & Quantile-based & Mean & Median & Mode\\
\hline
20 & 10 & 0.102 & 0.054 & 0.055 & 0.055\\
\hline
50 & 10 & 0.083 & 0.020 & 0.020 & 0.020\\
\hline
100 & 10 & 0.074 & 0.010 & 0.010 & 0.010\\
\hline
200 & 10 & 0.070 & 0.005 & 0.005 & 0.005\\
\hline
20 & 20 & 0.306 & 0.105 & 0.108 & 0.110\\
\hline
50 & 20 & 0.298 & 0.054 & 0.054 & 0.056\\
\hline
100 & 20 & 0.292 & 0.037 & 0.036 & 0.038\\
\hline
200 & 20 & 0.288 & 0.028 & 0.026 & 0.027\\
\hline
20 & 30 & 0.692 & 0.317 & 0.317 & 0.324\\
\hline
50 & 30 & 0.679 & 0.208 & 0.204 & 0.211\\
\hline
100 & 30 & 0.692 & 0.200 & 0.196 & 0.202\\
\hline
200 & 30 & 0.686 & 0.182 & 0.177 & 0.183\\
\hline
\end{tabular}
\end{table}

![](comparative-study-for-Outlier-Handling-methods_files/figure-latex/unnamed-chunk-3-4.pdf)<!-- --> \begin{table}

\caption{\label{tab:unnamed-chunk-3}Table 5: The proportion of samples are fitted by normal distribution at 0.05 level of significance after handling outliers.}
\centering
\begin{tabular}[t]{r|r|r|r|r|r}
\hline
n & epsilon & Quantile-based & Mean & Median & Mode\\
\hline
20 & 10 & 0.887 & 0.946 & 0.931 & 0.903\\
\hline
50 & 10 & 0.466 & 0.946 & 0.916 & 0.880\\
\hline
100 & 10 & 0.056 & 0.922 & 0.881 & 0.812\\
\hline
200 & 10 & 0.000 & 0.818 & 0.754 & 0.638\\
\hline
20 & 20 & 0.561 & 0.870 & 0.814 & 0.747\\
\hline
50 & 20 & 0.048 & 0.704 & 0.574 & 0.492\\
\hline
100 & 20 & 0.000 & 0.384 & 0.248 & 0.162\\
\hline
200 & 20 & 0.000 & 0.059 & 0.024 & 0.009\\
\hline
20 & 30 & 0.324 & 0.676 & 0.565 & 0.504\\
\hline
50 & 30 & 0.004 & 0.298 & 0.166 & 0.121\\
\hline
100 & 30 & 0.000 & 0.029 & 0.006 & 0.003\\
\hline
200 & 30 & 0.000 & 0.000 & 0.000 & 0.000\\
\hline
\end{tabular}
\end{table}

![](comparative-study-for-Outlier-Handling-methods_files/figure-latex/unnamed-chunk-3-5.pdf)<!-- --> 

### Negative Binomial Distribution
in Negative Binomial distribution we focused on probability of success, we calculated bias and mean squared error for estimator after applying the four different methods of handling outliers, with different sample sizes and different contamination levels, also we applied  Goodness of fit test namely Kolmogorov-Smirnov Test to determines if sample follows a Negative Binomial distribution

\begin{table}

\caption{\label{tab:unnamed-chunk-4}Table 6: Bias of the negative binomial Distribution probability of success estimator for different handling-outliers methods}
\centering
\begin{tabular}[t]{r|r|r|r|r|r}
\hline
n & epsilon & Quantile-based & Mean & Median & Mode\\
\hline
20 & 10 & 0.031 & 0.001 & 0.004 & 0.007\\
\hline
50 & 10 & 0.034 & 0.004 & 0.000 & 0.005\\
\hline
100 & 10 & 0.034 & 0.004 & 0.000 & 0.005\\
\hline
200 & 10 & 0.034 & 0.005 & 0.000 & 0.005\\
\hline
20 & 20 & 0.056 & 0.035 & 0.031 & 0.027\\
\hline
50 & 20 & 0.056 & 0.037 & 0.033 & 0.030\\
\hline
100 & 20 & 0.056 & 0.040 & 0.036 & 0.033\\
\hline
200 & 20 & 0.056 & 0.041 & 0.038 & 0.035\\
\hline
20 & 30 & 0.070 & 0.064 & 0.062 & 0.060\\
\hline
50 & 30 & 0.071 & 0.068 & 0.067 & 0.067\\
\hline
100 & 30 & 0.070 & 0.070 & 0.070 & 0.070\\
\hline
200 & 30 & 0.071 & 0.071 & 0.071 & 0.071\\
\hline
\end{tabular}
\end{table}

![](comparative-study-for-Outlier-Handling-methods_files/figure-latex/unnamed-chunk-4-1.pdf)<!-- --> \begin{table}

\caption{\label{tab:unnamed-chunk-4}Table 7: MSE of the negative binomial Distribution probability of success estimator for different handling-outliers methods}
\centering
\begin{tabular}[t]{r|r|r|r|r|r}
\hline
n & epsilon & Quantile-based & Mean & Median & Mode\\
\hline
20 & 10 & 0.001 & 0.001 & 0.001 & 0.002\\
\hline
50 & 10 & 0.001 & 0.001 & 0.001 & 0.001\\
\hline
100 & 10 & 0.001 & 0.000 & 0.000 & 0.000\\
\hline
200 & 10 & 0.001 & 0.000 & 0.000 & 0.000\\
\hline
20 & 20 & 0.003 & 0.002 & 0.003 & 0.003\\
\hline
50 & 20 & 0.003 & 0.002 & 0.002 & 0.002\\
\hline
100 & 20 & 0.003 & 0.002 & 0.002 & 0.002\\
\hline
200 & 20 & 0.003 & 0.002 & 0.002 & 0.001\\
\hline
20 & 30 & 0.005 & 0.005 & 0.005 & 0.005\\
\hline
50 & 30 & 0.005 & 0.005 & 0.005 & 0.005\\
\hline
100 & 30 & 0.005 & 0.005 & 0.005 & 0.005\\
\hline
200 & 30 & 0.005 & 0.005 & 0.005 & 0.005\\
\hline
\end{tabular}
\end{table}

![](comparative-study-for-Outlier-Handling-methods_files/figure-latex/unnamed-chunk-4-2.pdf)<!-- --> \begin{table}

\caption{\label{tab:unnamed-chunk-4}Table 8: The proportion of samples are fitted by Negative binomial distribution at 0.05 level of significance after handling outliers.}
\centering
\begin{tabular}[t]{r|r|r|r|r|r}
\hline
n & epsilon & Quantile-based & Mean & Median & Mode\\
\hline
20 & 10 & 0.598 & 0.874 & 0.826 & 0.774\\
\hline
50 & 10 & 0.280 & 0.642 & 0.532 & 0.434\\
\hline
100 & 10 & 0.076 & 0.416 & 0.334 & 0.276\\
\hline
200 & 10 & 0.010 & 0.244 & 0.196 & 0.108\\
\hline
20 & 20 & 0.466 & 0.676 & 0.612 & 0.578\\
\hline
50 & 20 & 0.110 & 0.366 & 0.284 & 0.232\\
\hline
100 & 20 & 0.014 & 0.160 & 0.082 & 0.060\\
\hline
200 & 20 & 0.000 & 0.052 & 0.014 & 0.006\\
\hline
20 & 30 & 0.376 & 0.458 & 0.458 & 0.418\\
\hline
50 & 30 & 0.090 & 0.084 & 0.074 & 0.076\\
\hline
100 & 30 & 0.006 & 0.008 & 0.004 & 0.006\\
\hline
200 & 30 & 0.000 & 0.000 & 0.000 & 0.000\\
\hline
\end{tabular}
\end{table}

![](comparative-study-for-Outlier-Handling-methods_files/figure-latex/unnamed-chunk-4-3.pdf)<!-- --> 



### Exponential distribution
Exponential distribution has only one parameter $\lambda$, we calculated bias and mean squared error for  estimator after applying the four different methods of handling outliers, with different sample sizes and different contamination levels, also we applied  Goodness of fit test namely Chi-Square Test to determines if sample follows a Exponential distribution

\begin{table}

\caption{\label{tab:unnamed-chunk-5}Table 9: Bias of the exponential distribution rate estimator for different handling-outliers methods}
\centering
\begin{tabular}[t]{r|r|r|r|r|r|r}
\hline
n & epsilon & Bias Rate Before & Quantile-based & Mean & Median & Mode\\
\hline
20 & 10 & 0.382 & 0.125 & 0.143 & 0.072 & 0.122\\
\hline
50 & 10 & 0.377 & 0.106 & 0.117 & 0.061 & 0.109\\
\hline
100 & 10 & 0.375 & 0.103 & 0.109 & 0.055 & 0.105\\
\hline
200 & 10 & 0.374 & 0.100 & 0.104 & 0.051 & 0.104\\
\hline
20 & 20 & 0.403 & 0.224 & 0.225 & 0.041 & 0.127\\
\hline
50 & 20 & 0.404 & 0.215 & 0.215 & 0.025 & 0.109\\
\hline
100 & 20 & 0.404 & 0.210 & 0.210 & 0.018 & 0.107\\
\hline
200 & 20 & 0.403 & 0.208 & 0.207 & 0.015 & 0.105\\
\hline
20 & 30 & 0.411 & 0.391 & 0.347 & 0.306 & 0.296\\
\hline
50 & 30 & 0.414 & 0.333 & 0.280 & 0.054 & 0.049\\
\hline
100 & 30 & 0.414 & 0.327 & 0.269 & 0.013 & 0.106\\
\hline
200 & 30 & 0.414 & 0.322 & 0.266 & 0.001 & 0.124\\
\hline
\end{tabular}
\end{table}

![](comparative-study-for-Outlier-Handling-methods_files/figure-latex/unnamed-chunk-5-1.pdf)<!-- --> \begin{table}

\caption{\label{tab:unnamed-chunk-5}Table 10: MSE of the exponential distribution rate estimator for different handling-outliers methods}
\centering
\begin{tabular}[t]{r|r|r|r|r|r|r}
\hline
n & epsilon & MSE Rate Before & Quantile-based & Mean & Median & Mode\\
\hline
20 & 10 & 0.146 & 0.025 & 0.025 & 0.030 & 0.049\\
\hline
50 & 10 & 0.142 & 0.015 & 0.016 & 0.013 & 0.024\\
\hline
100 & 10 & 0.141 & 0.012 & 0.013 & 0.008 & 0.017\\
\hline
200 & 10 & 0.140 & 0.011 & 0.011 & 0.005 & 0.014\\
\hline
20 & 20 & 0.163 & 0.056 & 0.053 & 0.022 & 0.046\\
\hline
50 & 20 & 0.163 & 0.048 & 0.047 & 0.007 & 0.022\\
\hline
100 & 20 & 0.163 & 0.045 & 0.045 & 0.003 & 0.017\\
\hline
200 & 20 & 0.163 & 0.044 & 0.043 & 0.001 & 0.014\\
\hline
20 & 30 & 0.170 & 0.153 & 0.122 & 0.099 & 0.094\\
\hline
50 & 30 & 0.172 & 0.112 & 0.080 & 0.018 & 0.034\\
\hline
100 & 30 & 0.172 & 0.108 & 0.073 & 0.004 & 0.022\\
\hline
200 & 30 & 0.172 & 0.104 & 0.071 & 0.001 & 0.020\\
\hline
\end{tabular}
\end{table}

![](comparative-study-for-Outlier-Handling-methods_files/figure-latex/unnamed-chunk-5-2.pdf)<!-- --> 


### Presentation of indicators
### Discussion
###Comments 



# CONCLUSION


# REFERENCES


1. https://www.redalyc.org/pdf/2990/299023509004.pdf
2. 
