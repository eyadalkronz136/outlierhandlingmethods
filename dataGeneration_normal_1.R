

library(robust)
library(dplyr)
library(ggplot2)
library(MLmetrics )
library(SimDesign)
library(tidyr)

# Create the function.
getmode <- function(data) {
  
  x<-data
  lim.inf=min(x)-1; lim.sup=max(x)+1
  
  # hist(x,freq=FALSE,breaks=seq(lim.inf,lim.sup,0.2))
  s<-density(x,from=lim.inf,to=lim.sup,bw=0.2)
  n<-length(s$y)
  v1<-s$y[1:(n-2)];
  v2<-s$y[2:(n-1)];
  v3<-s$y[3:n]
  ix<-1+which((v1<v2)&(v2>v3))
  
 
  md <- s$x[which(s$y==max(s$y))] 
  
  md
}


getUpperAndLower <- function(data) {
  q1 <- quantile(data  , c(.25))[["25%"]]
  q3 <- quantile(data  , c(.75))[["75%"]]
  IQR <- q3-q1
  lower  = q1 - 1.5*IQR
  upper = q3 + 1.5*IQR
  
  
  list(
    q1 = q1,
    q3 = q3 ,
    IQR = IQR ,
    lower = lower ,
    upper = upper 
  )
  
}

set.seed(1)
cont_norm <- function(n, # sample size
                      mu1 = 0, # only one mu since the mean is the same for both distributions. 
                      mu2 = 0, # only one mu since the mean is the same for both distributions. 
                      sd1 = 1, # sd of the first Distribution 
                      sd2 = 100, # sd of the second Distribution
                      prob = 0.1 # contamination proportion
){
  
  data <-  rnorm(n, mean = mu1, sd = sd1)
  
  list_contamination <-  rnorm(round((n*prob)), mean = mu2, sd = sd2)
 
  data_contamination <- c(data, list_contamination) 
  
  list(
    data = data,
    data_contamination = data_contamination
  )
  
}


generateData <- function(sampleSize , var , contamination ){
  
  
  results = NULL
  #create 1000 random sample with contamination
  for(i in 1:1000){
    #generate data  
    
    allData <- cont_norm(sampleSize,
                         mu1 = 0, 
                         mu2 = 4, 
                         sd1 = var,
                         sd2 = var*2,
                         prob =contamination)
    
    data_with_contamination <-  allData$data_contamination
    originalData <-  allData$data
    
    
    
    
    
    #Code To Handling outlier 
    
    
    #Quantile based flooring and capping
    #In this technique, the outlier is capped at a certain value above the upper percentile value or floored   at a factor below the lower percentile value.
    
    
    q1 <- quantile(data_with_contamination  , c(.25))[["25%"]]
    q3 <- quantile(data_with_contamination  , c(.75))[["75%"]]
    IQR <- q3-q1
    lower  = q1 - 1.5*IQR
    upper = q3 + 1.5*IQR
    
    outliers <- boxplot(data_with_contamination, plot=FALSE)$out
    outliersPos <- which(data_with_contamination %in% outliers)
    data_AfterDelete_Outliers <-  data_with_contamination[-outliersPos]
    
    
    dataAfterHandling_Q_b_F_C <- data_with_contamination
    dataAfterHandling_mean <- data_with_contamination
    dataAfterHandling_median <- data_with_contamination
    dataAfterHandling_mode <- data_with_contamination
    
    
    dataAfterHandling_Q_b_F_C[dataAfterHandling_Q_b_F_C<lower] <- lower
    dataAfterHandling_Q_b_F_C[dataAfterHandling_Q_b_F_C>upper] <- upper
    x_bar_After_Handling_Q_b_F_C <- mean(dataAfterHandling_Q_b_F_C)
    x_sd_After_Handling_Q_b_F_C <- sd(dataAfterHandling_Q_b_F_C)
    # 
    # 
    mean = mean(data_AfterDelete_Outliers)
    dataAfterHandling_mean[outliersPos] <- mean
    # dataAfterHandling_mean[dataAfterHandling_mean>upper] <- mean
    x_bar_After_mean <- mean(dataAfterHandling_mean)
    x_sd_After_mean <- sd(dataAfterHandling_mean)
    # 
    # 
    median = median(data_AfterDelete_Outliers)
    dataAfterHandling_median[outliersPos] <- median
    # dataAfterHandling_median[dataAfterHandling_median>upper] <- median
    x_bar_After_median <- mean(dataAfterHandling_median)
    x_sd_After_median <- sd(dataAfterHandling_median)
    # 
    mode = getmode(dataAfterHandling_mode)
    dataAfterHandling_mode[outliersPos] <- mode
    # dataAfterHandling_mode[dataAfterHandling_mode>upper] <- mode
    x_bar_After_mode <- mean(dataAfterHandling_mode)
    x_sd_After_mode <- sd(dataAfterHandling_mode)
    
    
    
    
    # Normality Test After Quantile based flooring and capping
    NormalityTest_p_value_After_Q_b_F_C <- shapiro.test(dataAfterHandling_Q_b_F_C)$p.value
    NormalityTest_p_value_After_mean <- shapiro.test(dataAfterHandling_mean)$p.value
    NormalityTest_p_value_After_median <- shapiro.test(dataAfterHandling_median)$p.value
    NormalityTest_p_value_After_mode <- shapiro.test(dataAfterHandling_mode)$p.value
    
    NormalityTest_p_value_Before_Handling <- shapiro.test(data_with_contamination)$p.value
    NormalityTest_p_value_OriginalData <- shapiro.test(originalData)$p.value
    
    
    
    results = rbind(
      results,
      data.frame(
        i,
        
        NormalityTest_p_value_After_Q_b_F_C ,
        NormalityTest_p_value_After_mean ,
        NormalityTest_p_value_After_median ,
        NormalityTest_p_value_After_mode ,
        NormalityTest_p_value_Before_Handling,
        NormalityTest_p_value_OriginalData,
        
        
        x_bar_After_Handling_Q_b_F_C,
        x_bar_After_mean,
        x_bar_After_median,
        x_bar_After_mode ,
        
        x_sd_After_Handling_Q_b_F_C,
        x_sd_After_mean,
        x_sd_After_median,
        x_sd_After_mode
      ))
  }
  
  results
}

####################################################################################
########################## Gnerate Data ###########################################

 


# different sample size with sd = 1 and contamination=5%
data_20_1_5 <- generateData(20 , 1,0.05)
write.csv(data_20_1_5,"Normal_Data\\data_20_1_5.csv", row.names = TRUE)

data_50_1_5 <- generateData(50 , 1,0.05)
write.csv(data_50_1_5,"Normal_Data\\data_50_1_5.csv", row.names = TRUE)

data_100_1_5 <- generateData(100,1,0.05)
write.csv(data_100_1_5,"Normal_Data\\data_100_1_5.csv", row.names = TRUE)

data_200_1_5 <- generateData(200,1,0.05)
write.csv(data_200_1_5,"Normal_Data\\data_200_1_5.csv", row.names = TRUE)


# different sample size with sd = 1 and contamination=10%
data_20_1_10 <- generateData(20 , 1 , .1)
write.csv(data_20_1_10,"Normal_Data\\data_20_1_10.csv", row.names = TRUE)

data_50_1_10 <- generateData(50 , 1 , .1)
write.csv(data_50_1_10,"Normal_Data\\data_50_1_10.csv", row.names = TRUE)

data_100_1_10 <- generateData(100 , 1 , 0.1)
write.csv(data_100_1_10,"Normal_Data\\data_100_1_10.csv", row.names = TRUE)

data_200_1_10 <- generateData(200 , 1 , 0.1)
write.csv(data_200_1_10,"Normal_Data\\data_200_1_10.csv", row.names = TRUE)



# different sample size with sd = 1 and contamination=15%
data_20_1_15 <- generateData(20 , 1,0.15)
write.csv(data_20_1_15,"Normal_Data\\data_20_1_15.csv", row.names = TRUE)

data_50_1_15 <- generateData(50 , 1,0.15)
write.csv(data_50_1_15,"Normal_Data\\data_50_1_15.csv", row.names = TRUE)

data_100_1_15 <- generateData(100,1,0.15)
write.csv(data_100_1_15,"Normal_Data\\data_100_1_15.csv", row.names = TRUE)

data_200_1_15 <- generateData(200,1,0.15)
write.csv(data_200_1_15,"Normal_Data\\data_200_1_15.csv", row.names = TRUE)

 



